// Quiz:
// 1. What is the term given to unorganized code that's very hard to work with?
// Answer: Spaghetti code

// 2. How are object literals written in JS?
// Answer: 
/*
	const myObject = {
	  key1: value1,
	  key2: value2,
	};
*/

// 3. What do you call the concept of organizing information and functionality to belong to an object?
// Answer: OOP - Object Oriented Programming

// 4. If the studentOne object has a method named enroll(), how would you invoke it?
// Answer: studentOne.enroll();

// 5. True or False: Objects can have objects as properties.
// Answer: True
const person = {
  firstName: "John",
  lastName: "Doe",
  address: {
    street: "123 Main St",
    city: "Exampleville",
    country: "Exampleland"
  }
};

console.log(person.firstName);          // Output: John
console.log(person.address.street);     // Output: 123 Main St
console.log(person.address.city);       // Output: Exampleville

// 6. What is the syntax in creating key-value pairs?
// Answer: key: value

// 7. True or False: A method can have no parameters and still work.
// Answer: True
function greet() {
  console.log("Hello, world!");
}
greet(); // Output: Hello, world!

// 8. True or False: Arrays can have objects as elements.
// Answer: True
const people = [
  { name: "Alice", age: 25 },
  { name: "Bob", age: 30 },
  { name: "Charlie", age: 28 }
];

// 9. True or False: Arrays are objects.
// Answer: True

// 10. True or False: Objects can have arrays as properties.
// Answer: True

// Function coding:

//without the use of objects, our students from before would be organized as follows if we are to record additional information about them

//create student one
let studentOneName = 'John';
let studentOneEmail = 'john@mail.com';
let studentOneGrades = [89, 84, 78, 88];

//create student two
let studentTwoName = 'Joe';
let studentTwoEmail = 'joe@mail.com';
let studentTwoGrades = [78, 82, 79, 85];

//create student three
let studentThreeName = 'Jane';
let studentThreeEmail = 'jane@mail.com';
let studentThreeGrades = [87, 89, 91, 93];

//create student four
let studentFourName = 'Jessie';
let studentFourEmail = 'jessie@mail.com';
let studentFourGrades = [91, 89, 92, 93];

//actions that students may perform will be lumped together
function login(email){
    console.log(`${email} has logged in`);
}

function logout(email){
    console.log(`${email} has logged out`);
}

function listGrades(grades){
    grades.forEach(grade => {
        console.log(grade);
    })
}

//This way of organizing students is not well organized at all.
//This will become unmanageable when we add more students or functions
//To remedy this, we will create objects

// 1. Translate the other students from our boilerplate code into their own respective objects.

// 2. Define a method for EACH student object that will compute for their grade average (total of grades divided by 4)

// 3. Define a method for all student objects named willPass() that returns a Boolean value indicating if student will pass or fail. For a student to pass, their ave. grade must be greater than or equal to 85.

// 4. Define a method for all student objects named willPassWithHonors() that returns true if ave. grade is greater than or equal to 90, false if >= 85 but < 90, and undefined if < 85 (since student will not pass).

// 5. Create an object named classOf1A with a property named students which is an array containing all 4 student objects in it.

// 6. Create a method for the object classOf1A named countHonorStudents() that will return the number of honor students.

// 7. Create a method for the object classOf1A named honorsPercentage() that will return the % of honor students from the batch's total number of students.

// 8. Create a method for the object classOf1A named retrieveHonorStudentInfo() that will return all honor students' emails and ave. grades as an array of objects.

// 9. Create a method for the object classOf1A named sortHonorStudentsByGradeDesc() that will return all honor students' emails and ave. grades as an array of objects sorted in descending order based on their grade averages.

let studentOne = {
    name: "John",
    email: "john@mail.com",
    grades: [89, 84, 78, 88],
    computeAve: function() {
        let total = this.grades.reduce((sum, grade) => sum + grade, 0);
        return total / this.grades.length;
    },
    willPass: function() {
        return this.computeAve() >= 85;
    },
    willPassWithHonors: function() {
        let average = this.computeAve();
        if (average >= 90) {
            return true;
        } else if (average >= 85) {
            return false;
        } else {
            return undefined;
        }
    }
};

let studentTwo = {
    name: "Joe",
    email: "joe@mail.com",
    grades: [78, 82, 79, 85],
    computeAve: function() {
        let total = this.grades.reduce((sum, grade) => sum + grade, 0);
        return total / this.grades.length;
    },
    willPass: function() {
        return this.computeAve() >= 85;
    },
    willPassWithHonors: function() {
        let average = this.computeAve();
        if (average >= 90) {
            return true;
        } else if (average >= 85) {
            return false;
        } else {
            return undefined;
        }
    }
};

let studentThree = {
    name: "Jane",
    email: "jane@mail.com",
    grades: [87, 89, 91, 93],
    computeAve: function() {
        let total = this.grades.reduce((sum, grade) => sum + grade, 0);
        return total / this.grades.length;
    },
    willPass: function() {
        return this.computeAve() >= 85;
    },
    willPassWithHonors: function() {
        let average = this.computeAve();
        if (average >= 90) {
            return true;
        } else if (average >= 85) {
            return false;
        } else {
            return undefined;
        }
    }
};

let studentFour = {
    name: "Jessie",
    email: "jessie@mail.com",
    grades: [91, 89, 92, 93],
    computeAve: function() {
        let total = this.grades.reduce((sum, grade) => sum + grade, 0);
        return total / this.grades.length;
    },
    willPass: function() {
        return this.computeAve() >= 85;
    },
    willPassWithHonors: function() {
        let average = this.computeAve();
        if (average >= 90) {
            return true;
        } else if (average >= 85) {
            return false;
        } else {
            return undefined;
        }
    }
};

let classOf1A = {
    students: [studentOne, studentTwo, studentThree, studentFour],
    countHonorStudents: function() {
        let honorStudentCount = 0;
        for (let student of this.students) {
            if (student.willPassWithHonors() === true) {
                honorStudentCount++;
            }
        }
        return honorStudentCount;
    },
     honorsPercentage: function() {
        let totalStudents = this.students.length;
        let honorStudentCount = this.countHonorStudents();
        return (honorStudentCount / totalStudents) * 100;
    },
    retrieveHonorStudentInfo: function() {
        let honorStudentInfo = [];

        for (let student of this.students) {
            if (student.willPassWithHonors() === true) {
                honorStudentInfo.push({
                    email: student.email,
                    averageGrade: student.computeAve()
                });
            }
        }

        return honorStudentInfo;
    },
    sortHonorStudentsByGradeDesc: function() {
        let honorStudentInfo = this.retrieveHonorStudentInfo();

        honorStudentInfo.sort((a, b) => b.averageGrade - a.averageGrade);

        return honorStudentInfo;
    }
};
