// Quiz
// 1. How do you create arrays in JS?
 const num = [1, 2, 3, 4];
// 2. How do you access the first character of an array?
const firstNum = num[0]; // 1
// 3. How do you access the last character of an array?
const lastNum = num[num.length -1]; // 4
// 4. What array method searches for, and returns the index of a given value in an array? This method returns -1 if given value is not found in the array.
const indexOfTwo = num.indexOf(2); // 1
const indexOfFive = num.indexOf(5); // -1
// 5. What array method loops over all elements of an array, performing a user-defined function on each iteration?
num.forEach(function(number) {
	console.log(number * 2)
})
// 6. What array method creates a new array with elements obtained from a user-defined function?
const doubleNumbers = num.map(function(number) {
	return number * 2;
})
// 7. What array method checks if all its elements satisfy a given condition?
const allEven = num.every(function(number) {
	return number % 2 === 0;
})
// 8. What array method checks if at least one of its elements satisfies a given condition?
const hasEvenNumber = num.some(function(number) {
	return number % 2 === 0;
})
// 9. True or False: array.splice() modifies a copy of the array, leaving the original unchanged.
// False
const removedElements = num.splice(1, 2); // Remove 2 elements starting from index 1
console.log(num); // Output: [1, 4] (original array modified)
console.log(removedElements); // Output: [2, 3] (removed elements)
// 10. True or False: array.slice() copies elements from original array and returns them as a new array.
// True
const slicedArray = num.slice(0, 1); // Get elements from index 1 to index 3 (end index is exclusive)

console.log(slicedArray); // Output: [1] (new array with copied elements)
console.log(num); // Output: [1, 4] (original array unchanged)

// Function Coding
// 1. Create a function named addToEnd that will add a passed in string to the end of a passed in array. If element to be added is not a string, return the string "error - can only add strings to an array". Otherwise, return the updated array. Use the students array and the string "Ryan" as arguments when testing.
const students = ["Alice", "Bob", "Charlie"];

function addToEnd(arr, element) {
    if (typeof element !== 'string') {
        return "error - can only add strings to an array";
    }
    
    arr.push(element);
    return arr;
}

// Testing with the students array and the string "Ryan"
const updatedStudents = addToEnd(students, "Ryan");
console.log(updatedStudents); // Output: ["Alice", "Bob", "Charlie", "Ryan"]

// 2. Create a function named addToStart that will add a passed in string to the start of a passed in array. If element to be added is not a string, return the string "error - can only add strings to an array". Otherwise, return the updated array. Use the students array and the string "Tess" as arguments when testing.
function addToStart(arr, element) {
    if (typeof element !== 'string') {
        return "error - can only add strings to an array";
    }
    
    arr.unshift(element);
    return arr;
}

// Testing with the students array and the string "Tess"
const updatedStudents2 = addToStart(students, "Tess");
console.log(updatedStudents2); // Output: ["Tess", "Alice", "Bob", "Charlie"]

// 3. Create a function named elementChecker that will check a passed in array if at least one of its elements has the same value as a passed in argument. If array is empty, return the message "error - passed in array is empty". Otherwise, return a boolean value depending on the result of the check. Use the students array and the string "Jane" as arguments when testing.
function elementChecker(arr, value) {
    if (arr.length === 0) {
        return "error - passed in array is empty";
    }

    return arr.includes(value);
}

// Testing with the students array and the string "Jane"
const result = elementChecker(students, "Jane");
console.log(result); // Output: false

// 4. Create a function named checkAllStringsEnding that will accept a passed in array and a character. The function will do the ff:
	// if array is empty, return "error - array must NOT be empty"
	// if at least one array element is NOT a string, return "error - all array elements must be strings"
	// if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string"
	// if 2nd argument is more than 1 character long, return "error - 2nd argument must be a single character"
	// if every element in the array ends in the passed in character, return true. Otherwise return false.
	// Use the students array and the character "e" as arguments when testing.
	// Tip - string characters may be accessed by passing in an index just like in arrays.
function checkAllStringsEnding(arr, character) {
    if (arr.length === 0) {
        return "error - array must NOT be empty";
    }

    if (!arr.every(element => typeof element === 'string')) {
        return "error - all array elements must be strings";
    }

    if (typeof character !== 'string') {
        return "error - 2nd argument must be of data type string";
    }

    if (character.length !== 1) {
        return "error - 2nd argument must be a single character";
    }

    return arr.every(element => element.endsWith(character));
}

// Testing with the students array and the character "e"
const result2 = checkAllStringsEnding(students, "e");
console.log(result2); // Output: false

// 5. Create a function named stringLengthSorter that will take in an array of strings as its argument and sort its elements in an ascending order based on their lengths. If at least one element is not a string, return "error - all array elements must be strings". Otherwise, return the sorted array. Use the students array to test.
function stringLengthSorter(stringsArray) {
    if (!stringsArray.every(element => typeof element === 'string')) {
        return "error - all array elements must be strings";
    }

    stringsArray.sort((a, b) => a.length - b.length);

    return stringsArray;
}

// Testing with the students array
const sortedStudents = stringLengthSorter(students);
console.log(sortedStudents); // Output: ['Bob', 'Tess', 'Ryan', 'Alice', 'Charlie']

// 6. Create a function named startsWithCounter that will take in an array of strings and a single character. The function will do the ff:
	// if array is empty, return "error - array must NOT be empty"
	// if at least one array element is NOT a string, return "error - all array elements must be strings"
	// if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string"
	// if 2nd argument is more than 1 character long, return "error - 2nd argument must be a single character"
	// return the number of elements in the array that start with the character argument, must be case-insensitive
	// Use the students array and the character "J" as arguments when testing.
function startsWithCounter(stringsArray, character) {
    if (stringsArray.length === 0) {
        return "error - array must NOT be empty";
    }

    if (!stringsArray.every(element => typeof element === 'string')) {
        return "error - all array elements must be strings";
    }

    if (typeof character !== 'string') {
        return "error - 2nd argument must be of data type string";
    }

    if (character.length !== 1) {
        return "error - 2nd argument must be a single character";
    }

    const lowercaseCharacter = character.toLowerCase();
    const count = stringsArray.filter(element => element.toLowerCase().startsWith(lowercaseCharacter)).length;

    return count;
}

// Testing with the students array and the character "J"
const students2 = ["Alice", "Bob", "Charlie", "David", "Eva", "John", "Joe"];
const count = startsWithCounter(students2, "j");
console.log(count); // Output: 2

// 7. Create a function named likeFinder that will take in an array of strings and a string to be searched for. The function will do the ff:
	// if array is empty, return "error - array must NOT be empty"
	// if at least one array element is NOT a string, return "error - all array elements must be strings"
	// if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string"
	// return a new array containing all elements of the array argument that contain the string argument in it, must be case-insensitive
	// Use the students array and the string "jo" as arguments when testing.

function likeFinder(stringsArray, searchString) {
    if (stringsArray.length === 0) {
        return "error - array must NOT be empty";
    }

    if (!stringsArray.every(element => typeof element === 'string')) {
        return "error - all array elements must be strings";
    }

    if (typeof searchString !== 'string') {
        return "error - 2nd argument must be of data type string";
    }

    const lowercaseSearchString = searchString.toLowerCase();
    const foundElements = stringsArray.filter(element => element.toLowerCase().includes(lowercaseSearchString));

    return foundElements;
}

// Testing with the students array and the string "jo"
const students3 = ["Alice", "Bob", "Charlie", "David", "Eva", "John", "Joe"];
const foundNames = likeFinder(students3, "jo");

console.log(foundNames); // Output: ["John", "Joe"]

// 8. Create a function named randomPicker that will take in an array and output any one of its elements at random when invoked. Pass in the students array as an argument when testing.
function randomPicker(array) {
    const randomIndex = Math.floor(Math.random() * array.length);
    return array[randomIndex];
}

// Testing with the students array
// const students = ["Alice", "Bob", "Charlie", "David", "Eva", "John", "Jenna"];
const randomStudent = randomPicker(students3);

console.log(randomStudent); // Output: A random element from the students array



